'use strict';

!function ($) {
	var map = void 0;
	var groups = {};
	var addresses = [];
	var savedMarkers = [];
	var activeGroup = null;
	var navContainer = $('#pointsOfSeilNav');
	var pointsSlider = $('#pointsSlider');
	var form = $('#pointsOfSeilForm');
	var citySelect = $('#pointsOfSeilSelect');
	var wrapper = $('.sellpoint-wrapper');

	window.initVektaMap = function () {
		map = new google.maps.Map(document.getElementById('pointsOfSeil'), {
			center: { lng: 37.498053, lat: 55.819287 },
			zoom: 11,
			styles: window.vektaMapStyle || []
		});
		getPoints(null, true);
		$('#pointsOfSeilMyLocation').on('click', getPoints);
	};

	pointsSlider.slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		fade: true,
		infinite: false
	});

	function groupBy(xs, key) {
		return xs.reduce(function (rv, x) {
			(rv[x[key]] = rv[x[key]] || []).push(x);
			return rv;
		}, {});
	}

	function getPoints(e, flag) {
		console.log("e, flag: ", e, flag);
		wrapper.height(wrapper.height());
		pointsSlider.slick('slickRemove', null, null, true);
		var options = {
			data: form.serialize(),
			method: form.attr('method'),
			url: form.attr('action'),
			dataType: 'json'
		};

		var request = $.ajax(options);
		request.then(function (response) {
			groups = groupBy(response, 'name');
			addresses = response;

			showPlacemarks();
			if (!flag) {
				createNav();
			}
			wrapper.height("");
		}).catch(function (err) {
			return console.log("err: ", err);
		});
	}

	function cleanPlacemarks() {
		savedMarkers.forEach(function (n) {
			return n.setMap(null);
		});
		savedMarkers = [];
	}

	function getInfoWindowContent(data) {
		var str = '';

		str += '' + data.address;

		if (data.tel) {
			str += '<br />\u0422\u0435\u043B\u0435\u0444\u043E\u043D: <b>' + data.tel + '</b>';
		}

		if (data.email) {
			str += '<br />\u042D\u043B.\u043F\u043E\u0447\u0442\u0430: <a href="mailto:' + data.email + '">' + data.email + '</a>';
		}

		return '\n\t\t\t<div class="wysiwyg">\n\t\t\t\t<h4>' + data.name + '</h4>\n\t\t\t\t<p>' + str + '</p>\n\t\t\t</div>\n\t\t';
	}

	var mapInfoWindow = null;

	function showPlacemarks(name) {
		if (!map || !map.center) return;
		var data = name ? addresses.filter(function (n) {
			return n.name === name;
		}) : addresses;
		if (savedMarkers.length) {
			cleanPlacemarks();
		}
		var bounds = new google.maps.LatLngBounds();

		data.forEach(function (n) {
			var myLatLng = new google.maps.LatLng(n.coords[0], n.coords[1]);
			var marker = new google.maps.Marker({
				position: myLatLng,
				map: map
			});

			if (mapInfoWindow) {
				mapInfoWindow = null;
			}

			mapInfoWindow = new google.maps.InfoWindow({
				map: map,
				content: getInfoWindowContent(n)
			});

			marker.addListener('mouseover', function () {
				mapInfoWindow.open(map, marker);
			});
			bounds.extend(myLatLng);
			savedMarkers.push(marker);
			marker.setMap(map);
		});

		map.fitBounds(bounds);
	}

	function createNav() {
		var btns = [];
		for (var name in groups) {
			var itemWrapper = $('<div />', {
				class: 'sellpoint__item-wrapper'
			});

			var sliderWrapper = $('<div />', {
				class: 'sellpoint__mobile-inner'
			});

			var item = $('<button/>', {
				type: 'button',
				text: name,
				class: 'sellpoint__btn'
			});

			item.on('click', placeNavHandler.bind(item, name));
			btns.push(itemWrapper.append(item, sliderWrapper));
		}
		navContainer.html(btns);
	}

	function renderMobileSlider(container, name) {
		var existingSlider = $(container).find('.sellpoint__mobile-slider');
		if (existingSlider.length !== 0) {
			existingSlider.slick("setPosition", 0);
			return;
		} else {
			// Слайдера еще нет, создаем его
			var MobileSlider = $('<div />', {
				class: 'sellpoint__mobile-slider'
			});

			groups[name].forEach(function (n) {
				var slide = createCard(n);
				MobileSlider.append(slide);
			});

			$(container).find('.sellpoint__mobile-inner').append(MobileSlider);
			setTimeout(function () {
				MobileSlider.slick({
					arrows: false,
					dots: true
				});
			}, 100);
		}
	}

	function placeNavHandler(name) {
		var itemWrapper = $(this).closest(".sellpoint__item-wrapper");
		$(itemWrapper).addClass('is-active').siblings().removeClass('is-active');
		wrapper.height(wrapper.height());
		showPlacemarks(name);
		createCards(name);
		renderMobileSlider(itemWrapper, name);
	}

	function createCards(name) {
		pointsSlider.slick('slickRemove', null, null, true);

		setTimeout(function () {
			groups[name].forEach(function (n) {
				var slide = createCard(n);
				pointsSlider.slick('slickAdd', slide);
				pointsSlider.slick('goTo', 0);
				wrapper.height("");
			});
		}, 0);
	}

	function createCard(card) {
		var tmpl = $('#templateCard').clone();
		tmpl.removeAttr('id');
		tmpl.removeAttr('style');

		var text = tmpl.find('[data-slot="text"]');
		var tel = tmpl.find('[data-slot="tel"]');
		var email = tmpl.find('[data-slot="email"]');
		var address = tmpl.find('[data-slot="address"]');
		var name = tmpl.find('[data-slot="name"]');

		name.html(card.name);
		name.parent().addClass('is-active');

		if (card.text) {
			text.html(card.text);
			text.parent().addClass('is-active');
		}

		if (card.tel) {
			tel.html(card.tel);
			tel.parent().addClass('is-active');
		}

		if (card.email) {
			email.html('<a href="mailto:' + card.email + '">' + card.email + '</email>');
			email.parent().addClass('is-active');
		}

		if (card.address) {
			address.html(card.address);
			address.parent().addClass('is-active');
		}
		return tmpl;
	}
}(jQuery);
//# sourceMappingURL=../javascripts/pointsofseil.js.map
