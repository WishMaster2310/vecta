(() => {
  // findIndex polyphil
  if (!Array.prototype.findIndex) {
    Array.prototype.findIndex = function (predicate) {
      if (this == null) {
        throw new TypeError(
          "Array.prototype.findIndex called on null or undefined"
        );
      }
      if (typeof predicate !== "function") {
        throw new TypeError("predicate must be a function");
      }
      var list = Object(this);
      var length = list.length >>> 0;
      var thisArg = arguments[1];
      var value;

      for (var i = 0; i < length; i++) {
        value = list[i];
        if (predicate.call(thisArg, value, i, list)) {
          return i;
        }
      }
      return -1;
    };
  }
})();

$(() => {
  const SLICK_ARROWS = {
    prevArrow:
      '<div class="slider-arrow slider-arrow--prev"><i class="i-vekta i-vekta--chevron-left"></i></div>',
    nextArrow:
      '<div class="slider-arrow slider-arrow--next"><i class="i-vekta i-vekta--shevron-right"></i></div>',
  };

  $(".hero__slider-item").each((idx, el) => {
    const bg = $(el).attr("data-sm-bg");
    if (bg) {
      $(el).append(
        `<div class="hero__slider-mobile-bg" style="background-image: url(${bg})"></div>`
      );
    }
  });

  $(".news-slider")
    .slick({
      slidesToShow: 4,
      slidesToScroll: 4,
      prevArrow: SLICK_ARROWS.prevArrow,
      nextArrow: SLICK_ARROWS.nextArrow,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false,
            dots: true,
          },
        },
        {
          breakpoint: 790,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false,
            dots: true,
          },
        },
        {
          breakpoint: 620,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
          },
        },
      ],
    })
    .on("setPosition", function (event, slick) {
      slick.$slides.css("height", slick.$slideTrack.height() + "px");
    });

  $('input[type="tel"]').mask("+7 (999) 999-99-99");

  $(".hero__slider")
    .slick({
      arrows: false,
      dots: true,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 2000,
    })
    .on("setPosition", function (event, slick) {
      slick.$slides.css("height", slick.$slideTrack.height() + "px");
    });

  $(".viewed-slider").slick({
    slidesToShow: 4,
    slidesToScroll: 4,
    prevArrow: SLICK_ARROWS.prevArrow,
    nextArrow: SLICK_ARROWS.nextArrow,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          arrows: false,
          dots: true,
        },
      },
      {
        breakpoint: 790,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          arrows: false,
          dots: true,
        },
      },
      {
        breakpoint: 620,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: true,
        },
      },
    ],
  });

  const maskedInputsCollection = $("input[data-masked]");

  maskedInputsCollection.each((idx, el) => {
    const pattern = $(el).attr("data-masked");
    $(el).mask(pattern);
  });

  /**
   * Function Accordion Handler
   * @params opts {Object} - options
   * @params e {Event} - clickEvent
   **/
  function accordionHandler(opts, e) {
    e.preventDefault();
    const handler = $(e.target);
    const { parentClass, contentClass } = opts;
    const block = handler.closest(parentClass);
    const content = block.find(contentClass).first();
    const isActive = block.hasClass("is-active");

    if ($(content).length === 0) return;

    if (isActive) {
      $(content)
        .stop(true, true)
        .slideUp(300, () => block.removeClass("is-active"));
    } else {
      $(content)
        .stop(true, true)
        .slideDown(300, () => block.addClass("is-active"));
    }
  }

  $(".has-accordion .filter__title").on(
    "click",
    accordionHandler.bind(null, {
      parentClass: ".filter__section",
      contentClass: ".filter__body",
    })
  );

  $(".accordion__title").on(
    "click",
    accordionHandler.bind(null, {
      parentClass: ".accordion",
      contentClass: ".accordion__body",
    })
  );

  function cartHoverUnLock(e) {
    $(this).closest(".card").removeClass("hover-lock");
  }
  function cartHoverLock(e) {
    $(this).closest(".card").addClass("hover-lock");
  }

  $(".card__icon-bar")
    .on("mouseenter", cartHoverLock)
    .on("mouseleave", cartHoverUnLock);

  // product slider
  (function () {
    const slider = $(".product-slider__thumbs");
    const thumbs = $(".product-slider__thumb");
    const wrapper = $(".product-slider__image-base");
    const baseImage = $(".product-slider__image-base img");
    const gallery = [];
    let activeSlide = 0;

    thumbs.each((idx, el) => {
      const iframe = $(el).attr("data-iframe");
      $(el).attr("data-slide", idx);
      gallery.push({
        src: iframe ? iframe : $(el).attr("data-src"),
        type: iframe ? "iframe" : "image",
        opts: {
          thumb: $(el).find("img").attr("src"),
        },
      });
    });

    baseImage.data("gallery", gallery);

    const nextArrow = $("<div />", {
      class: "product-slider__arrow product-slider__arrow--next",
    });
    const prevArrow = $("<div />", {
      class: "product-slider__arrow product-slider__arrow--prev",
    });

    slider.on("init", () => {
      thumbsHandler.call(thumbs[0]);
      slider.closest(".product-slider").append(nextArrow, prevArrow);
    });

    slider.slick({
      vertical: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      verticalSwiping: true,
    });

    slider.on("afterChange", (e, slick, current) => {
      console.log(slick.$slides);
      const el = slick.$slides[current];
      // $(el).find('.product-slider__thumb').trigger('click')
    });

    function handleClickNext() {
      const next = activeSlide < thumbs.length - 1 ? activeSlide + 1 : 0;
      slider.slick("slickNext");
      thumbsHandler.call(thumbs[next]);
    }

    function handleClickPrev() {
      const next = activeSlide === 0 ? thumbs.length - 1 : activeSlide - 1;
      slider.slick("slickPrev");
      thumbsHandler.call(thumbs[next]);
    }

    nextArrow.on("click", handleClickNext);
    prevArrow.on("click", handleClickPrev);

    function thumbsHandler(e) {
      e && e.preventDefault();
      const self = $(this);
      activeSlide = Number(self.attr("data-slide")) || 0;
      if (self.hasClass("is-active")) return;
      const src = self.attr("data-src");
      const iframe = self.attr("data-iframe");
      const existingIframe = wrapper.find("iframe");

      /**
       * Если новый слайд это не фрейм,
       * то удаляем ранее вставленный
       */
      if (!iframe && existingIframe.length) {
        existingIframe.remove();
      }

      baseImage.stop(true, true).fadeOut(300, () => {
        if (!iframe) {
          setSliderImage(src, baseImage, setActiveThumb(self));
        } else {
          setSliderIframe(iframe, existingIframe, setActiveThumb(self));
        }
      });
    }

    function setActiveThumb(current) {
      $(".product-slider__thumb").not(current).removeClass("is-active");
      current.addClass("is-active");
    }

    function setSliderIframe(source, target, callback) {
      const frame =
        !target || target.length === 0 ? createSliderIframe() : target;
      frame.attr("src", source);
      frame.stop(true, true).fadeIn(300);

      if (callback && typeof callback === "function") {
        callback();
      }
    }

    function createSliderIframe() {
      const frame = $("<iframe />", {
        frameBorder: 0,
        allowFullScreen: true,
        class: "product-slider__iframe",
      });
      wrapper.append(frame);
      return frame;
    }

    function setSliderImage(source, target, callback) {
      const image = new Image();
      image.src = source;
      image.onload = function () {
        target.attr("src", source).fadeIn();

        if (callback && typeof callback === "function") {
          callback();
        }
      };
    }

    $(document).on("click", ".product-slider__thumb", thumbsHandler);

    function openGallery() {
      const currentSrc = $(this).attr("src");
      const activeIndex = gallery.findIndex((el) => el.src === currentSrc);

      $.fancybox.open(
        gallery,
        {
          loop: true,
          zoom: false,
          baseClass: "vekta-gallery",
          buttons: ["close"],
          thumbs: {
            autoStart: true,
            parentEl: ".fancybox-container",
            axis: "x",
          },
          afterShow: function (instance, current) {
            if (!current.$image.parent().hasClass("product-slider__zoom")) {
              current.$image
                .wrap('<span class="product-slider__zoom"></span>')
                .parent()
                .zoom();
            }
          },
        },
        activeIndex
      );
    }
    baseImage.on("click", openGallery);
  })();

  (function () {
    const tab = $(".tab");
    const tabNav = $(".tab__nav-item");
    const tabUnits = $(".tab__unit");

    function onTabShown(e) {
      const _tab = $(e.target);
      const _slider = tab.find(".slick-slider");

      if (_slider.length) {
        _slider.slick("setPosition", 0);
      }
    }

    function tabNavHandler(e) {
      e && e.preventDefault();
      const self = $(this);
      const target = self.attr("href");

      if ($(target).length) {
        self
          .addClass("is-active")
          .siblings(".tab__nav-item")
          .removeClass("is-active");
        $(target)
          .addClass("is-active")
          .siblings(".tab__unit")
          .removeClass("is-active");
        $(target).trigger("tabs:shown");
      }
    }

    tabNav.on("click", tabNavHandler);
    tabUnits.on("tabs:shown", onTabShown);
  })();

  (function () {
    const removeBtn = $(".myproducts__remove");
    const form = $("#removeProductForm");
    const input = $("#removeProductId");

    function checkOnEmpty() {
      const table = $(".myproducts__table tbody tr");
      return table.length === 0;
    }

    function removeHandler(e) {
      e.preventDefault();
      const row = $(this).closest("tr");
      input.val($(this).attr("data-product"));

      $.ajax({
        method: form.attr("method"),
        url: form.attr("action"),
        data: form.serialize(),
      })
        .then((response) => {
          input.val("");
          row.remove();

          if (checkOnEmpty()) {
            $(".myproducts__table").css("display", "none");
            $(".myproducts-empty").fadeIn();
          }
        })
        .catch((err) => {
          console.log("err: ", err);
        });
    }
    removeBtn.on("click", removeHandler);
  })();

  (function () {
    function overHandler() {
      const sublist = $(this).find(".subcategories");
      const wrapper = $(this).closest(".tile-list-wrapper");
      const self = $(this);
      const tile = self.find(".tile");

      if (!sublist.length) return;
      const width = $(window).width();
      const top =
        self.offset().top - $(wrapper).offset().top + self.outerHeight();
      tile.addClass("is-hovered");
      sublist.css({ width, top });
      sublist.stop(true, true).fadeIn();
    }

    function outHandler() {
      const tile = $(this).find(".tile");
      const sublist = $(this).find(".subcategories");
      tile.removeClass("is-hovered");

      if (!sublist.length) return;
      sublist.stop(true, true).fadeOut();
    }

    function subOverHandler() {
      const wrapper = $(this).closest(".subcategories");
      const text = $(wrapper).find(".subcategories__label span");
      const title = $(this).attr("title");
      text.text(title);
    }

    function subOutHandler() {
      const wrapper = $(this).closest(".subcategories");
      const text = $(wrapper).find(".subcategories__label span");
      text.text("");
    }

    $(".subcategories__link")
      .on("mouseenter", subOverHandler)
      .on("mouseleave", subOutHandler);

    $(".tile-list__item")
      .on("mouseenter", overHandler)
      .on("mouseleave", outHandler);
  })();

  /**
   * scroll to top btn
   **/
  (function () {
    const btn = $(".scroll-top");

    function scrollToTop() {
      $("html, body").animate(
        {
          scrollTop: 0,
        },
        400
      );
    }

    function scrollHandler() {
      const scrTop = $(window).scrollTop();
      const h = $(window).height();
      if (scrTop > h * 0.75) {
        btn.stop(true, true).fadeIn();
      } else {
        btn.stop(true, true).fadeOut();
      }
    }

    $(window).on("scroll", $.throttle(scrollHandler, 200));
    btn.on("click", scrollToTop);
  })();

  $(".js-print").on("click", (e) => window.print());

  $(".custom-select").SumoSelect();

  function openModal(e) {
    e.preventDefault();
    const instance = $.fancybox.getInstance();
    const modal = $(this).attr("data-modal");
    if (!modal) return;

    if (instance) {
      instance.close();
      setTimeout(() => {
        $.fancybox.open({
          src: modal,
        });
      }, 0);
    } else {
      $.fancybox.open({
        src: modal,
      });
    }
  }

  $(".js-openmodal").on("click", openModal);

  // header
  !(function () {
    const btn = $(".hamburger");
    const wrapper = $('<div class="mobile-nav"></div>');

    function openerHandler(e) {
      e.preventDefault();
      const target = $(this).siblings(".hidden-list");
      $(this).parent().toggleClass("is-open");
      $(target).length > 0 && $(target).stop(true, true).slideToggle(300);
    }

    function getSubmenuDropdown(node) {
      return node
        .find(".dropmenu__unit")
        .not(".is-disabled")
        .not(".is-hidden-mobile")
        .map((idx, el) => {
          const hasList = $(el).find(".dropmenu__link").length > 0;
          const link = $(el).find(".dropmenu__title");

          const result = $("<div />", {
            class: "mobile-nav__subitem",
          });

          const mobileLink = $("<a />", {
            text: $(link).text(),
            href: $(link).attr("href"),
            class: "mobile-nav__group",
          });

          result.append(mobileLink);

          if (hasList) {
            result.append("<button class='mobile-nav__toggle'></button>");
            mobileLink.addClass("has-list");
            const inner = $("<div>", {
              class: "mobile-nav__groups hidden-list",
            });

            inner.append(
              $(el)
                .find(".dropmenu__link")
                .map((j, subEl) => {
                  return $("<a />", {
                    href: $(subEl).attr("href"),
                    text: $(subEl).text(),
                    class: "mobile-nav__groups-item",
                  });
                })
                .get()
            );
            result.append(inner);
          }

          return result;
        });
    }

    const mobileNavContent = $(".header__nav-item").map((idx, el) => {
      const section = $("<div />", {
        class: "mobile-nav__item",
      });
      const dropDown = $(el).attr("data-dropdown");
      const link = $("<a />", {
        href: $(el).attr("href"),
        class: "mobile-nav__item-link",
        html: $(el).html(),
      });

      if ($(el).hasClass("is-active")) {
        link.addClass("is-active");
      }

      section.append(link);

      if (dropDown) {
        link.addClass("has-list");
        section.append("<button class='mobile-nav__toggle'></button>");
        const node = $(`[data-dropdown-id="${dropDown}"]`);
        if (node.length > 0) {
          // const btn = $("<button />", { class: 'mobile-nav__opener' });
          // section.append(btn);
          const inner = $("<div />", {
            class: "mobile-nav__list hidden-list",
          });
          section.append(inner.append(getSubmenuDropdown(node).get()));
        }
      }
      return section;
    });

    wrapper.append(mobileNavContent.get());

    if (!$(".mobile-nav").length) {
      $("body").append(wrapper);
      $(".mobile-nav__toggle").on("click", openerHandler);
    }

    function clickHandler() {
      if ($(this).hasClass("is-active")) {
        $(this).removeClass("is-active");
        $("body").removeClass("is-mobile-open");
      } else {
        $(this).addClass("is-active");
        $("body").addClass("is-mobile-open");
      }
    }
    btn.on("click", clickHandler);
  })();

  !(function () {
    const handler = $(".js-searchtoggle");
    const searchBox = $(".searchbox");
    const activeClass = "is-active";
    let searchState = false;
    function hideSearchBox() {
      handler.removeClass(activeClass);
      searchBox.removeClass(activeClass);
      searchBox.stop(true, true).fadeOut();
      searchState = false;
    }

    function showSearchBox() {
      const posX =
        $(window).innerWidth() - handler.offset().left - handler.outerWidth();
      const posY = handler.offset().top + handler.outerHeight() + 10;
      handler.addClass(activeClass);
      searchBox.addClass(activeClass);
      searchBox.css({ right: posX, top: posY });
      searchBox.stop(true, true).fadeIn(400);
      searchState = true;
    }

    function searchToggle(e) {
      e.stopPropagation();
      const isActive = $(this).hasClass(activeClass);
      if (isActive) hideSearchBox();
      else showSearchBox();
    }

    $(document).on("click", (e) => {
      if (!searchState) return true;
      if (!$(e.target).closest(".searchbox").length) {
        hideSearchBox();
      }
    });

    $(window).on("resize", () => {
      if (!searchState) return true;
      else hideSearchBox();
    });

    handler.on("click", searchToggle);
  })();

  /**
   * Dropdown menu
   */
  !(function () {
    function unTheme(el) {
      const regex = /(theme-\S+)/gm;
      const str = el.attr("class");
      const prevTheme = regex.exec(str);
      if (prevTheme) {
        el.removeClass(prevTheme[0]);
      }
    }

    function unitMouseover(e) {
      const dMenu = $(".dropmenu.is-active");
      let theme = $(this).attr("data-theme") || "default";
      unTheme(dMenu);
      dMenu.addClass(`theme-${theme}`);
    }

    $(".dropmenu__unit").on("mouseenter", unitMouseover);

    function hideDropmenuHandler() {
      const menu = $(".dropmenu.is-active");
      if (!menu.length) return true;
      unTheme(menu);
      menu.removeClass("is-active").stop(true, true).fadeOut(400);
      $(document).off("click", hideDropmenuHandler);
      return true;
    }

    function showDropMenu(dropdown) {
      const menu = $(`[data-dropdown-id="${dropdown}"]`);
      if (!menu.length) return;
      hideDropmenuHandler();
      unTheme(menu);
      menu.addClass("is-active").stop(true, true).fadeIn(300);
      $(document).on("click", hideDropmenuHandler);
    }

    function menuPoiterHandler() {
      const dropdown = $(this).data("dropdown");
      if (dropdown) {
        showDropMenu(dropdown);
      } else {
        hideDropmenuHandler();
      }
    }

    if ($(window).outerWidth() > 1210) {
      $(".header__nav-item").on("mouseenter", menuPoiterHandler);
    }

    $(".dropmenu").on("mouseleave", hideDropmenuHandler);
  })();

  /**
   * Vekta popovers
   */
  !(function () {
    /**
     * Вытаскиваем контент, или из вложенной разметки
     * или из атрибута data-title
     * если и там и там пусто, то ничего не выводим
     */
    function getContent(el) {
      const html = $(el).find(".vekta-tooltip__content");
      if (html.length) return html.html();
      const title = $(el).attr("data-content");
      if (title) {
        return title;
      }
      return false;
    }

    function getTemplate() {
      const template = $("<div/>", {
        class: "vekta-tooltip-view",
      });
      return template;
    }

    function getPosition(el, tooltipW) {
      const h = el.height();
      const w = el.width();
      const left = el.offset().left;
      const top = el.offset().top;
      const wWidth = $(window).width();
      const topPlacement = left + tooltipW + 30 > wWidth;

      if (topPlacement) {
        return {
          left: 15,
          top: top - h,
        };
      } else {
        return {
          left: left + el.width() + 15,
          top: top + h,
        };
      }
    }

    function hidePopover() {
      const target = $(".vekta-tooltip-view.is-active");

      if (target.length) {
        target.fadeOut(200, function () {
          const parent = $(this).data("parent");
          parent.data("isOpen", false);
          $(parent).removeClass("is-active");
          target.remove();
          $(document).off("click", hidePopover);
        });
      }
      return true;
    }

    function showPopover(e) {
      e.preventDefault();
      const offset = $(this).offset();
      const isOpen = $(this).data("isOpen");
      $(this).addClass("is-active");

      if (!isOpen) {
        const content = getContent($(this));
        if (!content) return;
        $(this).data("isOpen", true);
        const tooltip = getTemplate();
        tooltip.append(content);
        const position = getPosition($(this), 320);
        tooltip.css({
          top: position.top,
          left: position.left,
        });
        tooltip.appendTo("body");
        tooltip.data("parent", $(this));

        $(tooltip).fadeIn(300, function () {
          $(document).on("click", hidePopover);
          tooltip.addClass("is-active");
        });
      }
    }

    $(".vekta-tooltip").on("click", showPopover);

    $(window).on("resize", () => {
      hidePopover();
    });
  })();

  (function () {
    const list = $(".glossary-nav__link");
    if (!list.length) return true;
    const options = list.toArray().map((node) => {
      const option = document.createElement("option");
      option.text = $(node).text();
      option.value = $(node).attr("href");
      return option;
    });
    const select = document.getElementById("glossarySelect");
    $(select).html(options);
    $(select).on("change", changeHandler);

    function changeHandler() {
      const name = ("" + $(this).val()).slice(1);
      const node = $(`.glossary-anchor[name="${name}"]`);
      if (node.length) {
        window.scrollTo(0, node.offset().top);
        window.location.hash = $(this).val();
      }
    }
  })();

  function toggleFilters() {
    $(".filter").toggleClass("is-active");
    return true;
  }

  $(".js-toggle-filters").on("click", toggleFilters);
});
